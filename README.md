# Test 2 / Klausur 2

Klonen Sie zuerst das Projekt https://stbaeumer@bitbucket.org/stbaeumer/n27gw12-klausur2-fehler-finden.git vom Bitbucket.

#### Es werden ausschließlich Ihre handschriftlichen Auswertungen auf diesem Zettel eingesammelt und bewertet!

Dokumentieren Sie hier unter Angabe der fehlerhaften Zeile in der entsprechenden Datei, welche Korrektur Sie vornehmen müssen, um den Fehler zu korrigieren: Schreiben sie das wie folgt: 
_server.js, Zeile 3, Es fehlt die schließende geschweifte Klammer. Die muss ergänzt werden._

### Aufgabe 1 - Fehler finden (Klausur- und Testschreiber)

Die graue Menüzeile ist durcheinander geraten. Klicken Sie zuerst auf das "Profil bearbeiten"-Icon. Dann sehen Sie ein Problem. Klicken Sie anschließend auf das Wort "Impressum" in der Menüleiste. Dann kommt es zu einem weiteren Problem, das aber mit dem ersten Problem zusammenhängt. 

Was ist das Problem? Wie sieht die Lösung aus?







Wenn Sie eine Überweisung tätigen, dann bekommen Sie eine Meldung zurückgegeben. Die Meldung ist fehlerhaft.
Was ist das Problem? Wie sieht die Lösung aus?









### Aufgabe 2 - Nur Klausurschreiber

Ein großer Vorteil der IBAN gegenüber der früheren Kombination aus Kontonummer und Bankleitzahl ist, dass die IBAN bereits beim Eingeben auf Richtigkeit überprüft werden kann. Das von uns verwendete IBAN-Modul kann nicht nur eine IBAN erstellen

```Javascript
// server.js, Zeile 148
konto.Iban = iban.fromBBAN(laenderkennung,bankleitzahl + " " + konto.Kontonummer)
```

sondern auch die Prüfung auf Richtigkeit durchführen. Die Dokumentation des IBAN-Moduls zeigt Ihnen, was die Funktion ```isValid()``` zurückgibt:

```Javascript

iban.isValid('falsche IBAN'); // false

iban.isValid('BE68539007547034'); // true

```

Bauen Sie die Prüfung in ihr Projekt ein! Wenn eine fehlerhafte IBAN eingegeben wird, soll bei Klick auf den Button "Überweisung ausführen" die Meldung "Fehlerhafte IBAN" auf der GUI angezeigt werden.



### Aufgabe 3 - Nur Klausurschreiber

Sie bekommen eine E-Mail von Max Mustermann. Am Ende der E-Mail steht eine sogenannte Signatur, also Angaben zum Absender:

```
Max Mustermann
Berufskolleg Borken
Josefstraße 10
46325 Borken
fon +49(0) 2861 90990-0
Public Key: <https://berufskolleg-borken.de/bm.asc>
```
Beschreiben Sie, was Sie tun müssen, beginnend bei der Installation bis zum endgültigen Versand, um Max Mustermann eine verschlüsselte Datei zu senden. Beschreiben Sie keine Tätigkeiten, die hier nicht von Bedeutung sind.


### Aufgabe 4 - Klausur- und Testschreiber

Nennen Sie für die Symmetrischen Verschlüsselung und die Asymmetrischen Verschlüsselung jeweils Vorteile. Beispiel:
* Die Symmetrische Verschlüsselung ist schneller.
* Die Asymmetrische Verschlüsselung macht es möglich, dass eine Datei gleichzeitig für mehrere Personen mit verschiedenen privaten Schlüsseln verschlüsselt werden kann. Jede dieser Personen kann dann mit dem eigenen Privaten Schlüssel entschlüsseln.








### Aufgabe 5 - Nur Klausurschreiber

Ein Klassenkamerad hat als Geschäftsidee ein Tool programmiert, das Dateien vor dem Speichern im G-Drive oder Onedrive automatisch verschlüsselt. Dadurch verbinden sich die Vorteile der Cloud mit der Anforderung des Datenschutzes. Damit trotzdem mehrere Leute gemeinsam eine Datei öffnen können, soll die Asymmetrische Verschlüsselung zum Einsatz kommen. Denn nur die Asymmetrische Verschlüsselung erlaubt es, dass eine Datei gleichzeitig für mehrere,  verschiedene Personen _mit verschiedenen privaten Schlüsseln_ verschlüsselt werden kann.

Leider ist die Asymmetrische Verschlüsselung sehr viel langsamer als die Symmetrische Verschlüsselung. Entwickeln Sie eine Idee für einen Lösungsansatz, der den Komfort der Asymmetrischen Verschlüsselung mit der Geschwindigkeit der Symmetrischen Verschlüsselung verbindet!

### Aufgabe 6 - Test- und Klausurschreiber

Schreiben Sie den Quelltext, der im Terminal den Wert von ```z``` unter Beachtung der Nebenbedingungen ausgibt.

* Wenn ```x``` größer als ```5``` ist und gleichzeitig ```y``` keine negative Zahl ist, dann wird der Wert von ```z``` verdoppelt. 
* Wenn ```x``` größer als ```5``` ist und gleichzeitig ```y``` eine negative Zahl ist, dann wird der Wert von ```z``` gleich ```0```. 
* In jedem anderen Fall wird der Wert von z um 5 erhöht.

```Javascript
let x = 5
let y = 3
let z = 1
```









### Aufgabe 7 - Klausurschreiber

Ein Objekt der Klasse Artikel ist mit den Eigenschaften *Bezeichnung*, *Art* und *Nettopreis* instanziiert worden. 
Die Eigenschaftswerte des Objekts ```artikel``` sind *Grimms Märchen*, *Buch*, *100*

Die Ausgaben, die Sie im Terminal generieren sollen lauten beispielhaft:

    ```"Sie haben Grimms Märchen gekauft (Preis: 107 Euro). Das Buch wird als Geschenk verpackt."```
    oder
    ```"Sie haben Samsung A10 gekauft (Preis: 1119 Euro)."```
    oder
    ```"Sie haben Node-Praxisbuch gekauft (Preis: 107 Euro). Das Buch wird nicht als Geschenk verpackt."```
    usw.

Die Nebenbedingen lauten:

* Bücher und Lebensmittel haben einen Mehrwertsteuersatz von 7 %, ansonsten gelten 19 %. 
* Der Bruttopreis muss entsprechend errechnet werden.
* Nur bei Büchern soll im Terminal stehen, ob sie als Geschenk verpackt werden oder nicht. Der Wert von ```geschenk``` ist beispielsweise ```false```. 

Schreiben Sie alle fehlenden Anweisungen auf, um die beispielhaften Ausgaben im Terminal anzuzeigen. 

```Javascript
let geschenk = false
```






### Aufgabe 8 - Nur Klausurschreiber

Bislang kennen Sie noch keine Anweisung, um eine Datenbank mit SQL abzufragen. Das Infoblatt liefert das notwendige Wissen, um folgende Abfrage in SQL zu formulieren.

Eine Tabelle namens ```Artikel``` hat die Eigenschaften ```Bezeichnung```, ```Art``` und ```Nettopreis```

Formulieren Sie die Abfrage der Tabelle ```Artikel```, wenn gilt, dass
* alle Artikel der Art ```Buch``` angezeigt werden sollen
* nur die verschiedenen ```Bezeichnungen``` (also ohne Doppelnennungen) angezeigt werden sollen





